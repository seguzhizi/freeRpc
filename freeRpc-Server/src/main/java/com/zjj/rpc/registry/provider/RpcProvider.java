package com.zjj.rpc.registry.provider;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.zjj.rpc.registry.util.AnnotationUtil;
import com.zjj.rpc.registry.util.ClassUtil;

public class RpcProvider {

	public static void main(String[] args) throws Exception {
//		HelloService service = new HelloServiceImpl();
//		RpcFramework.export(service, 1234);
		
		// 获取特定包下所有的类(包括接口和类)
//		Package  startPackage = RpcProvider.class.getPackage();
//		Package pcks = startPackage.getPackage("com");
//		System.out.println(pcks);
//		List<Class<?>> clsList = ClassUtil.getAllClassByPackageName("com");
		
		List<Class<?>> clsList = ClassUtil.getAllClassByPackageName(RpcProvider.class.getPackage());
		// 输出所有使用了特定注解的类的注解值
		List<String> classNameList = AnnotationUtil.validClassAnnotation(clsList);
		
		String classNameJson = JSONObject.toJSONString(classNameList);
		System.out.println("classNameJson = " + classNameJson);
		
		Socket socket = new Socket("127.0.0.1" , 2345);

		OutputStream os = socket.getOutputStream();
		byte[] bytes = classNameJson.getBytes("utf-8");
		os.write(bytes);
		os.flush();
		socket.shutdownOutput();
		
		InputStream is = socket.getInputStream();
		byte[] resultByte = new byte[1024];
		String response = "";
		while(is.read( resultByte) != -1) {
			response += new String(resultByte , "utf-8");
		}
		System.out.println(response);
		is.close();
		os.close();
		socket.close();
//		for(Map.Entry<String, Class<?>> entry : RegistryServiceHolder.serviceMap.entrySet()) {
//			System.out.println(entry.getKey() + "------" + entry.getValue());
//		}
		
		
	}

}
