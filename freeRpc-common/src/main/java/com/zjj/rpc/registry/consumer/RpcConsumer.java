package com.zjj.rpc.registry.consumer;

import com.zjj.rpc.registry.RpcFramework;
import com.zjj.rpc.registry.service.HelloService;

public class RpcConsumer {

	public static void main(String[] args) throws Exception {
		HelloService service = RpcFramework.refer(HelloService.class, "127.0.0.1", 1234);
		for (int i = 0; i < 3; i++) {
			String hello = service.hello("World" + i);
			System.out.println(hello);
			Thread.sleep(1000);
		}
	}

}
