package com.zjj.rpc.registry.provider;

import com.zjj.rpc.registry.RpcFramework;
import com.zjj.rpc.registry.service.HelloService;
import com.zjj.rpc.registry.service.impl.HelloServiceImpl;

//@ServiceProvider
public class RpcProvider {

	public static void main(String[] args) throws Exception {
		HelloService service = new HelloServiceImpl();
		RpcFramework.export(service, 1234);
	}

}
