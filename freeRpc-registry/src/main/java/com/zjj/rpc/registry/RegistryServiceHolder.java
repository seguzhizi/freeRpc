package com.zjj.rpc.registry;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.zjj.rpc.registry.common.dto.RegistryDto;

public class RegistryServiceHolder {
	
	
	public volatile static ConcurrentHashMap<String, RegistryDto> serviceMap = new ConcurrentHashMap<>();

	public static ConcurrentHashMap<String, RegistryDto> getServiceMap() {
		return serviceMap;
	}
	
	public static RegistryDto getService(String serviceName) {
		return serviceMap.get(serviceName);
	}

	public static void setService(Map<String , RegistryDto> serviceMap) {
		RegistryServiceHolder.serviceMap.putAll(serviceMap);
	}
	
	public static void setService(String className , RegistryDto clazz) {
		RegistryServiceHolder.serviceMap.put(className , clazz);
	}
	
	@Override
	public String toString() {
		return "";
	}
	
	

}
