package com.zjj.rpc.registry.util;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import com.zjj.rpc.registry.annotation.FreeRpcService;
 
 
public class AnnotationUtil {
	
	public static List<String> validClassAnnotation(List<Class<?>> clsList){
		List<String> classNameList = new ArrayList<>();
		if (clsList != null && clsList.size() > 0) {
			for (Class<?> clazz : clsList) {
				FreeRpcService  freeRpcService = clazz.getAnnotation(FreeRpcService.class);
				if(freeRpcService != null) {
					classNameList.add(clazz.getName());
//					RegistryServiceHolder.setService(clazz.getName() , clazz);
					
					System.out.println(freeRpcService.value());
				}
			}
		}
		return classNameList;
	}
	
	public static void validMethodAnnotation(List<Class<?>> clsList){
		if (clsList != null && clsList.size() > 0) {
			for (Class<?> cls : clsList) {
				//获取类中的所有的方法
				Method[] methods = cls.getDeclaredMethods();
				if (methods != null && methods.length > 0) {
					for (Method method : methods) {
						FreeRpcService freeRpcService = (FreeRpcService) method.getAnnotation(FreeRpcService.class);
						if (freeRpcService != null) {
							//可以做权限验证
							System.out.println(freeRpcService.value());
						}
					}
				}
			}
		}
	}
 
}